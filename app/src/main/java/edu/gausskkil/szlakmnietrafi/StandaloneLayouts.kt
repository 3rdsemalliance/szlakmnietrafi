package edu.gausskkil.szlakmnietrafi

import android.net.Uri
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.adaptive.AnimatedPane
import androidx.compose.material3.adaptive.ExperimentalMaterial3AdaptiveApi
import androidx.compose.material3.adaptive.ListDetailPaneScaffold
import androidx.compose.material3.adaptive.ListDetailPaneScaffoldRole
import androidx.compose.material3.adaptive.ThreePaneScaffoldNavigator
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.FileProvider
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.rememberImagePainter
import edu.gausskkil.szlakmnietrafi.ui.theme.Black
import java.util.Objects

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun MainLayout(modifier: Modifier = Modifier, navController: NavController, routeList: List<Route>, onClick: ((CurrentRoute) -> Unit)? = null) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.primary,
                ),
                title = {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Card{
                            Image(
                                painter = painterResource(id = R.drawable.kamil),
                                contentDescription = null,
                                contentScale = ContentScale.FillBounds,
                                alignment = Alignment.Center
                            )
                        }
                    }
                },
                scrollBehavior = scrollBehavior,
            )
        },
        modifier = modifier.nestedScroll(scrollBehavior.nestedScrollConnection)
//        bottomBar = {
//            Searchbar(routeList = routeList)
//        }
    ) { innerPadding ->
        // innerPadding contains inset information for you to use and apply
        LazyVerticalStaggeredGrid(
            columns = StaggeredGridCells.Fixed(2),
            // consume insets as scaffold doesn't do it by default
            modifier = Modifier.consumeWindowInsets(innerPadding),
            contentPadding = innerPadding,
//            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            items(routeList.size) { index ->
                if(onClick == null){
                    TrailCard(routeList[index].name,
                        modifier = modifier.clickable{
                            val arg = routeList[index].id.toString()
                            navController.navigate("DetailLayout/$arg")
                        })
                    HorizontalDivider(color = MaterialTheme.colorScheme.outline)
                }
                else {
                    TrailCard(
                        routeList[index].name,
                        modifier = modifier.clickable {
                            onClick(CurrentRoute(routeList[index].id))
                        }
                    )
                    HorizontalDivider(color = MaterialTheme.colorScheme.outline)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun DetailLayout(index: Int, modifier: Modifier, database: AppDatabase, timerViewModel: TimerViewModel){
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
    val context = LocalContext.current
    val file = context.createImageFile()
    val uri = FileProvider.getUriForFile(
        Objects.requireNonNull(context),
        BuildConfig.APPLICATION_ID + ".provider", file
    )
    var capturedImageUri by remember {
        mutableStateOf<Uri>(Uri.EMPTY)
    }
    val cameraLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) {
            capturedImageUri = uri
        }
    val permissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {
        if (it) {
            // Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
            cameraLauncher.launch(uri)
        } else {
            // Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }

    if(index == -1){
        Scaffold(
            topBar = {
                TopAppBar(
                    colors = TopAppBarDefaults.topAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        titleContentColor = MaterialTheme.colorScheme.primary,
                    ),
                    title = {
                        Text(text = "Detale", textAlign = TextAlign.Center)
                    }
                )
            },
        ) { innerPadding ->
            // innerPadding contains inset information for you to use and apply
            Column(
                // consume insets as scaffold doesn't do it by default
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize()
            ) {
                Text("Wybierz szlak", modifier = modifier.fillMaxSize(), fontSize = 36.sp, textAlign = TextAlign.Center)
            }
            FloatingActionButton(
                onClick = { checkPermission(context, cameraLauncher, uri, permissionLauncher) },
            ) {
                Icon(Icons.Filled.Add, "Floating action button.")
            }
        }
    }
    else{
        val routeInfo = database.RouteDAO().getDescForRoute(index)
        val routeTimesList = database.TimesDAO().getDescForRouteTime(index)
        val textPadding = 10.dp
        Scaffold(
            topBar = {
                TopAppBar(
                    colors = TopAppBarDefaults.topAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        titleContentColor = MaterialTheme.colorScheme.primary,
                    ),
                    title = {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Text(text = "Szczegóły szlaku", textAlign = TextAlign.Center)
                            Card{
                                Image(
                                    painter = painterResource(id = R.drawable.kamil),
                                    contentDescription = null,
                                    contentScale = ContentScale.FillBounds,
                                    alignment = Alignment.Center
                                )
                            }
                        }
                    },
                    scrollBehavior = scrollBehavior,
                )
            },
            modifier = modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { checkPermission(context, cameraLauncher, uri, permissionLauncher) },
                    modifier = Modifier.padding(15.dp)
                ) {
                    Icon(Icons.Filled.Add, "Floating action button.")
                }
            }
        ) { innerPadding ->
            // innerPadding contains inset information for you to use and apply
            LazyColumn(
                // consume insets as scaffold doesn't do it by default
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                item{
                    Text(routeInfo.name, modifier = modifier, fontSize = 36.sp, textAlign = TextAlign.Center, color = MaterialTheme.colorScheme.error, lineHeight = 48.sp)
                    Text("Opis szlaku", modifier = modifier, fontSize = 30.sp, textAlign = TextAlign.Center)
                    HorizontalDivider(modifier = Modifier.width(4.dp))
                    Text(
                        routeInfo.description,
                        modifier = modifier.padding(textPadding),
                        fontSize = 18.sp,
                        textAlign = TextAlign.Justify,
                    )
                    HorizontalDivider(
                        modifier = Modifier
                            .width(4.dp)
                            .padding(textPadding / 2), color = MaterialTheme.colorScheme.outline
                    )
                    Text("Dystans: ${routeInfo.distance}km", modifier = Modifier
                        .fillMaxWidth()
                        .padding(textPadding / 2), fontSize = 30.sp)
                    Text("Czas: ${routeInfo.time}", modifier = Modifier
                        .fillMaxWidth()
                        .padding(textPadding / 2), fontSize = 30.sp)
                    TimerScreenContent(timerViewModel = timerViewModel, database = database, currentRouteID = index)
                    if (capturedImageUri.path?.isNotEmpty() == true) {
                        Image(
                            modifier = Modifier
                                .padding(16.dp, 8.dp),
                            painter = rememberImagePainter(capturedImageUri),
                            contentDescription = null
                        )
                    }
                    else {
                        Log.i("Image", "Non image found in capturedImageUri")
                    }
                }
                item{
                    Text("Zobacz swoje poprzednie podejścia do tego szlaku:", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)
                }
                items(routeTimesList.size) { index ->
                    val routeName = database.RouteDAO().getDescForRoute(routeTimesList[index].routeID).name
                    val date = routeTimesList[index].date
                    val time = timeToText(routeTimesList[index].time)
                    Text("$routeName $date: $time", modifier = Modifier.padding(horizontal = 8.dp))
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RouteTimesLayout(modifier: Modifier = Modifier, navController: NavController, database: AppDatabase){
    val routeTimesList = database.TimesDAO().getAllRouteTimes()
    Scaffold() { innerPadding ->
        // innerPadding contains inset information for you to use and apply
        LazyColumn(
            // consume insets as scaffold doesn't do it by default
            modifier = Modifier.consumeWindowInsets(innerPadding),
            contentPadding = innerPadding,
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            items(routeTimesList.size) { index ->
                val routeName = database.RouteDAO().getDescForRoute(routeTimesList[index].routeID).name
                val date = routeTimesList[index].date
                val time = routeTimesList[index].time
                Text("$routeName $date $time")
            }
        }
    }
}


@Composable
fun Navigate(database: AppDatabase, timerViewModel: TimerViewModel, hardness: Boolean){
    val routeList =
        if (hardness) database.RouteDAO().getRoutesAfterGivenDistance(5.0)
        else database.RouteDAO().getRoutesToGivenDistance(5.0)
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "MainLayout") {
        composable(route = "MainLayout"){
            MainLayout(navController = navController, routeList = routeList)
        }
        composable(
            route = "DetailLayout" + "/{routeID}",
            arguments = listOf(
                navArgument("routeID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routeID")
                ?.let { DetailLayout(index = it, modifier = Modifier.fillMaxWidth(), database = database, timerViewModel = timerViewModel) }
        }
    }
}

@OptIn(ExperimentalMaterial3AdaptiveApi::class)
@Composable
fun ListDetailLayout(
    navigator: ThreePaneScaffoldNavigator,
    selectedRoute: MutableState<CurrentRoute?>,
    database: AppDatabase,
    navController: NavController,
    timerViewModel: TimerViewModel,
    hardness: Boolean){
    val routeList =
        if (!hardness) database.RouteDAO().getRoutesAfterGivenDistance(5.0)
        else database.RouteDAO().getRoutesToGivenDistance(5.0)
    ListDetailPaneScaffold(
        scaffoldState = navigator.scaffoldState,
        listPane = {
            AnimatedPane(modifier = Modifier) {
                MainLayout(onClick = {
                    selectedRoute.value = it
                    navigator.navigateTo(ListDetailPaneScaffoldRole.Detail)
                },
                    navController = navController, routeList = routeList
                )
            }
        },
        detailPane = {
            AnimatedPane(modifier = Modifier) {
                selectedRoute.value?.let { route ->
                    DetailLayout(index = route.id, modifier = Modifier, database = database, timerViewModel = timerViewModel)
                }
//                                DetailLayout(index = -1, modifier = Modifier, database = database)
            }
        }
    )
}