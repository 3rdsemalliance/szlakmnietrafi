package edu.gausskkil.szlakmnietrafi

import android.content.Context
import androidx.compose.runtime.saveable.Saver
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.Update

@Entity(tableName = "routes")
data class Route(
    @PrimaryKey val id: Int,
    val name: String,
    val distance: Double,
    val time: String,
    val description: String
)

@Entity(tableName = "routestimes")
data class RouteTime(
    @PrimaryKey val id: Int,
    val routeID: Int,
    val time: Long,
    val date: String
)

class CurrentRoute(var id: Int){
    companion object{
        val Saver: Saver<CurrentRoute?, Int> = Saver({it?.id}, ::CurrentRoute)
    }
}

class CurrentTabIndex(var id: Int){
    companion object{
        val Saver: Saver<CurrentTabIndex?, Int> = Saver({it?.id}, ::CurrentTabIndex)
    }
}

@Dao
interface RouteDAO {
    @Insert
    fun insert(route: Route)

    @Update
    fun update(route: Route)

    @Delete
    fun delete(route: Route)

    @Query("SELECT COUNT(*) FROM routes")
    fun getRouteCount(): Int

    @Query("SELECT * FROM routes")
    fun getAllRoutes(): List<Route>

    @Query("SELECT * FROM routes WHERE distance <= (:distance)")
    fun getRoutesToGivenDistance(distance: Double): List<Route>

    @Query("SELECT * FROM routes WHERE distance > (:distance)")
    fun getRoutesAfterGivenDistance(distance: Double): List<Route>

    @Query("SELECT * FROM routes where id = (:routeID)")
    fun getDescForRoute(routeID: Int): Route

    @Query("SELECT id FROM routes where name = (:routeName)")
    fun getIdFromName(routeName: String): Int

    @Query("delete from routes where 1=1")
    fun purge()
}

@Dao
interface TimesDAO {
    @Insert
    fun insert(routeTime: RouteTime)

    @Update
    fun update(routeTime: RouteTime)

    @Delete
    fun delete(routeTime: RouteTime)

    @Query("SELECT COUNT(*) FROM routestimes")
    fun getRouteTimesCount(): Int

    @Query("SELECT * FROM routestimes ORDER BY id ASC")
    fun getAllRouteTimes(): List<RouteTime>

    @Query("delete from routestimes where 1=1")
    fun purge()

    @Query("SELECT MAX(id)+1 FROM routestimes")
    fun genNextIndex(): Int

    @Query("SELECT * FROM routestimes where routeID = (:routeID)")
    fun getDescForRouteTime(routeID: Int): List<RouteTime>
}

@Database(entities = [Route::class, RouteTime::class], version = 3)
abstract class AppDatabase : RoomDatabase() {
    abstract fun RouteDAO(): RouteDAO
    abstract fun TimesDAO(): TimesDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "routes.db"
            ).build()
    }
}