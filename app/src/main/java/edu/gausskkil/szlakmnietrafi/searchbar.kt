import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.PopupProperties
import androidx.compose.ui.zIndex
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import edu.gausskkil.szlakmnietrafi.AppDatabase
import edu.gausskkil.szlakmnietrafi.Route
import edu.gausskkil.szlakmnietrafi.ui.theme.White
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import org.intellij.lang.annotations.JdkConstants

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun SearchbarScreen(searchbarView: SearchbarView, searchText: String, isSearching: Boolean, searchbarRouteList: List<Route>, navController: NavController, showPopupChange: (Boolean) -> Unit){
    SearchBar(
        query = searchText,
        onQueryChange = searchbarView::onSearchTextChange,
        onSearch = searchbarView::onSearchTextChange,
        active = isSearching,
        onActiveChange = { searchbarView.onToggleSearch() },
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        LazyColumn {
            items(searchbarRouteList) { route ->
                Row(modifier = Modifier.clickable{
                    Log.i("Search", "${route.id}")
                    navController.navigate("DetailLayout/${route.id}")
                    showPopupChange(false)
                }){
                    Text(
                        text = route.name,
                        modifier = Modifier
                            .padding(
                                start = 8.dp,
                                top = 4.dp,
                                end = 8.dp,
                                bottom = 4.dp
                            )
                    )
                }
            }
        }
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun SearchbarContent(searchbarView: SearchbarView, navController: NavController, showPopupChange: (Boolean) -> Unit){
    val searchText by searchbarView.searchText.collectAsState()
    val isSearching by searchbarView.isSearching.collectAsState()
    val searchbarRouteList by searchbarView.routesList.collectAsState()
    SearchbarScreen(searchbarView = searchbarView, searchText = searchText, isSearching = isSearching, searchbarRouteList = searchbarRouteList, navController = navController, showPopupChange = showPopupChange)
}

class SearchbarView(routesList: List<Route>) : ViewModel() {
    //first state whether the search is happening or not
    private val _isSearching = MutableStateFlow(false)
    val isSearching = _isSearching.asStateFlow()

    //second state the text typed by the user
    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    //third state the list to be filtered
    private val _routesList = MutableStateFlow(routesList)
    val routesList = searchText
        .combine(_routesList) { text, routes ->//combine searchText with _routesList
            if (text.isBlank()) { //return the entry list of countries if not is typed
                routes
            }
            routes.filter { route ->// filter and return a list of routes based on the text the user typed
                route.name.uppercase().contains(text.trim().uppercase())
            }
        }.stateIn(//basically convert the Flow returned from combine operator to StateFlow
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),//it will allow the StateFlow survive 5 seconds before it been canceled
            initialValue = _routesList.value
        )

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    fun onToggleSearch() {
        _isSearching.value = !_isSearching.value
        if (!_isSearching.value) {
            onSearchTextChange("")
        }
    }
}

@Composable
fun PopupSearchbar(showPopup: Boolean, shownContent: (Int) -> Unit, onClickOutside: () -> Unit, content: @Composable() () -> Unit) {
    if (showPopup) {
        // full screen background
        Box(
            modifier = Modifier
                .fillMaxWidth()
//                .height(500.dp)
                .background(White)
                .zIndex(10F),
            contentAlignment = Alignment.Center
        ) {
            // popup
            Popup(
                alignment = Alignment.Center,
                properties = PopupProperties(
                    excludeFromSystemGesture = false,
                ),
                // to dismiss on click outside
                onDismissRequest = { onClickOutside() }
            ) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                        .clip(RoundedCornerShape(4.dp)),
                    contentAlignment = Alignment.Center
                ) {
                    content()
                }
            }
        }
    }
}
