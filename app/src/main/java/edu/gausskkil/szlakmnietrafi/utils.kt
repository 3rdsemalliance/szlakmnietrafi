package edu.gausskkil.szlakmnietrafi

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.core.content.ContextCompat
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import android.Manifest
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DrawerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarState
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun populateDatabase(database: AppDatabase){
    if(database.RouteDAO().getRouteCount() == 0){
        database.RouteDAO().insert(Route(0, "Na Kasprowy Wierch z Kuźnic", 7.5, "2h30","Trasa na Kasprowy Wierch z Kuźnic należy do jednych z najłatwiejszych w polskich Tatrach. Jest monotonna i chwilami wręcz nudna, wszelako polecam ją z uwagi na brak dużego zatłoczenia szczególnie w okresie zimowym (duża część ludzi po prostu wjeżdża kolejką na szczyt). Ponadto stanowi wspaniałe miejsce do treningu dla biegaczy."))

        database.RouteDAO().insert(Route(1, "Przez Dolinę Kościeliską", 6.0, "1h30", "Bardzo przyjemna, wręcz spacerowa trasa. Biegnie przez gigantyczną Dolinę Kościeliską- jedną z najśliczniejszych w Tatrach. Oblegana latem i zimą przez rzesze turystów. Wiele ciekawych jaskiń (Mylna, Mroźna, Raptawicka, Miętusia i inne). Prowadzi do schroniska \"Ornak\", skąd można pójść na Smreczyński Staw, Iwaniacką Przełęcz i Czerwone Wierchy (przez Dolinę Tomanową)."))

        database.RouteDAO().insert(Route(2, "Na Zawracie z Polany Chochołowskiej", 7.2, "2h10", "Szlak zdecydowanie bardziej monotonny i mniej atrakcyjny widokowo od trasy przez Grzesia i Rakoń. Prowadzi przez Wyżnią Dolinę Chochołowską. Dopiero po wejściu na grań (25-30 minut przed zdobyciem szczytu) można zachwycać się wspaniałym krajobrazem części Tatr Zachodnich."))

        database.RouteDAO().insert(Route(3, "Na Halę Gąsienicową z Rusinowej Polany", 9.0, "2h45", "Bardzo długi szlak, który od skrzyżowania w Dolinie Pańszczycy rozgałęzia się. Jedna odnoga wiedzie generalnie w dół zboczem Żółtej Turni, zaś druga (niejako główna trasa) przez las. Przejście w obu przypadkach zajmie nam tyle samo czasu."))

        database.RouteDAO().insert(Route(4, "Na Mięguszowiecką Przełęcz pod Chłopkiem znad Morskiego Oka", 4.0, "2h10", "Pomijając Orlą Perć, szlak na Mięguszowiecką Przełęcz pod Chłopkiem uchodzi za najtrudniejszy w polskich Tatrach, aczkolwiek jest zdecydowanie krótszy i mniej męczący choćby od trasy na Rysy. Wiele wspinaczkowych fragmentów, nieliczne zabezpieczenia (tylko klamry, brak łańcuchów) i niezwykle duża ekspozycja najlepiej charakteryzują tę wymagającą tatrzańską perć, którą zdecydowanie należy odradzić turystom z jakimkolwiek lękiem wysokości."))

        database.RouteDAO().insert(Route(5, "Na Nosal z Kuźnic", 1.8, "0h50", "Pozornie łatwiejszy wariant wejścia na ten bardzo blisko Zakopanego położony i niezwykle popularny szczyt. Zarówno trasa z Kuźnic, jak i od przystanku \"Murowanica\" zajmują tyle samo czasu."))

        database.RouteDAO().insert(Route(6, "Na Nosal z Zakopanego (Murowanica)", 1.6, "0h55", "Trasa wiodąca od przystanku \"Murowanica\". Jest to nieco bardziej wymagający wariant wejścia na ten leżący tuż opodal Zakopanego szczyt. Trudy wędrówki wynagradzają nam dość szybko pojawiające się przepiękne widoki."))

        database.RouteDAO().insert(Route(7, "Na Wielki Kopieniec przez Dolinę Olczyską", 5.0, "1h30", "Trasa warta polecenia głównie z uwagi na świetny punkt widokowy, jakim niewątpliwie jest Wielki Kopieniec. Szlak bardzo łatwy, dostępny dla każdego w lecie, natomiast w zimie pewne kłopoty może sprawić tylko finałowe podejście na szczyt (i zejście z niego)."))

        database.RouteDAO().insert(Route(8, "Na Gęsią Szyję z Wierchu Poroniec", 4.5, "1h20", "Wierzchołek Gęsiej Szyi stanowi świetny punkt widokowy na polskie i słowackie Tatry Wysokie. Wycieczka na tę górę należy do zdecydowanie najłatwiejszych i popularniejszych, więc polecana jest praktycznie każdej osobie. Szlak stanowi idealną opcję na rozgrzewkę. Co ciekawe, szczyt można zdobyć z kilku stron. Poniższy opis prezentuje dojście z Wierchu Poroniec."))

        database.RouteDAO().insert(Route(9, "Na Halę Ornak z Polany Chochołowskiej", 7.7, "2h25", "Trasa wiedzie przez Iwaniacką Przełęcz. Nie ma szybszego sposobu dojścia ze schroniska na Polanie Chochołowskiej do sąsiedniego. Perć bardzo popularna i używana zarówno w okresie letnim, jak i zimowym. Nie można jednak zapomnieć o poważnym zagrożeniu lawinowym na odcinku za Przełęczą Iwaniacką."))

        database.RouteDAO().insert(Route(10, "Na Ciemniak z Hali Ornak przez Dolinę Tomanową", 7.0, "3h00", "Wspaniała pod względem widokowym, ale mało popularna, rzadko obierana i niejako niedoceniana trasa na Czerwone Wierchy. Co więcej, jest zdecydowanie mniej męcząca od podejścia przez Upłaziańską Kopkę. Panuje na niej cisza i spokój. Niemniej jednak należy wskazać, że startuje z Hali Ornak, do której najkrótsza droga wiedzie przez rozległą Dolinę Kościeliską, chyba że nocujemy w schronisku. Szlak (ściślej jego lwia część do Chudej Przełączki) zamknięty w sezonie zimowym, tj. od 1 grudnia do 15 maja."))

        database.RouteDAO().insert(Route(11, "Na Siwą Przełęcz przez Iwaniacką Przełęcz", 12.0, "3h45", "Bardzo ciekawa i nieco męcząca wycieczka, acz nad wyraz bezpieczna (lecz w zimie niedoświadczonym radzi się poprzestać na Iwaniackiej Przełęczy). Widoki zaczynają się już od osiągnięcia grani Ornaku."))

        database.RouteDAO().insert(Route(12, "Na Starorobociański Wierch z Siwej Polany przez Siwą Przełęcz", 12.5, "4h15", "Trasa czarnym szlakiem przez Dolinę Starorobociańską wydaje się mniej atrakcyjna widokowo od choćby tej wiodącej przez grzbiet Ornaku. Sam Starorobociański Wierch usytuowany jest w głównej grani Tatr Zachodnich i z daleka przypomina piramidę. Wejście na szczyt w porze letniej nie powinno przysporzyć żadnych kłopotów. Jednakże podczas finalnego podejścia należy zachować dużą ostrożność - urwiste zbocze po prawej stronie."))

        database.RouteDAO().insert(Route(13, "Droga pod Reglami", 12.5, "3h00", "Spacerowy szlak pod kilkoma wzniesieniami reglowymi (stąd nazwa). Dzięki temu, że rozciąga się poza granicami Tatrzańskiego Parku Narodowego, dopuszczalna jazda rowerem. Co więcej, nie musimy uiszczać żadnych opłat. Droga pod Reglami wiedzie prawie 7,5 km przez zalesione wzgórza, dlatego też podziwiać możemy jedynie panoramę Zakopanego i Pasma Gubałowskiego. Tatry Zachodnie, a tym bardziej Tatry Wysokie całkowicie zakryte. Trasę można również przejechać rowerem. Wycieczkę rozpoczynamy spod \"Księżówki\" w Kuźnicach, która znajduje się naprzeciwko przystanku \"Murowanica\" (po drugiej stronie ulicy). Naturalnie można wyruszyć także z Siwej Polany na peryferiach Doliny Chochołowskiej, aczkolwiek poniżej zostanie przedstawione przejście tylko z pierwszej wspomnianej strony."))

        database.RouteDAO().insert(Route(14, "Ściezka nad Reglami", 16.0, "5h45", "Słynny szlak reglowy. Wytyczony w latach 1900-01. Prowadzi przez większość dolin Tatr Zachodnich. Całkowita długość trasy wynosi około 16 km. Zatłoczona tylko w wybranych miejscach, głównie na wysokości Doliny Strążyskiej i w okolicy kilku punktów widokowych. Turyści najczęściej startują z Polany Kalatówki, rzadziej z Doliny Chochołowskiej. Poniżej opis tego popularniejszego przejścia."))

        database.RouteDAO().insert(Route(15, "Na Sarnią Skałę z Czerwonej Przełęczy", 0.5, "0h10", "Króciuteńki szlak dojściowy na bardzo popularny szczyt. Z Sarniej Skały niezwykle imponująco prezentuje się północna, spadzista ściana Giewontu."))

        database.RouteDAO().insert(Route(16, "Na Kozi Wierch z Doliny Pięciu Stawów Polskich", 3.0, "1h30", "Najprostsza trasa na najwyższy szczyt leżący w całości w Polsce. Brak jakichkolwiek sztucznych zabezpieczeń (bo i ich zamontowanie byłoby zupełnie niecelowe). Niewielkie trudności techniczne jedynie pod koniec (fragment Orlej Perci), dlatego szlak można polecić nawet początkującym. Zimą dużo narciarzy."))

        database.RouteDAO().insert(Route(17, "Na Halę Gąsienicową z Brzezin przez Dolinę Suchej Wody", 6.6, "1h40", "Droga przez Dolinę Suchej Wody jest bezsprzecznie najmniej ciekawa spośród wszystkich na Halę Gąsienicową, gdyż cały czas wiedzie gęstym lasem. Wydaje się jednak niezwykle bezpieczna, także w zimie (brak zagrożenia lawinowego). Do właściwego początku Doliny Suchej Wody najlepiej dotrzeć z Brzezin albo Toporowej Cyrhli."))

        database.RouteDAO().insert(Route(18, "Nad Smreczyński Staw z Hali Ornak", 1.5, "0h30", "Niewielki, acz często odwiedzany staw znajdujący się w dolnej części Doliny Pyszniańskiej. Znad brzegu wspaniały widok na Bystrą - położony na Słowacji największy szczyt Tatr Zachodnich."))

        database.RouteDAO().insert(Route(19, "Na Świnicę z Hali Gąsienicowej przez Zieloną Dolinę Gąsienicową", 5.3, "2h20", "Świnica to jeden z ciekawszych i bardziej popularnych szczytów w polskich Tatrach. Trasy, które prowadzą na wierzchołek, zaliczają się do trudnych. Zimą bywa ślisko. Co więcej, należy pamiętać o ogromnym niebezpieczeństwie podczas burzy w górnej kopule szczytowej (przebywanie w pobliżu licznych łańcuchów, w które uderzają pioruny, grozi śmiercią). Podejście od Świnickiej Przełęczy jest łatwiejsze od podejścia od strony Zawratu (mniej ekspozycji i wspinaczki)."))
    }
    Log.i("Rows", "${database.RouteDAO().getRouteCount()}")
}

fun checkPermission(
        context: Context,
        cameraLauncher: ManagedActivityResultLauncher<Uri, Boolean>,
        uri: Uri,
        permissionLauncher: ManagedActivityResultLauncher<String, Boolean>){
    val permissionCheckResult =
        ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
    if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
        cameraLauncher.launch(uri)
    } else {
        // Request a permission
        permissionLauncher.launch(Manifest.permission.CAMERA)
    }
}

@SuppressLint("SimpleDateFormat")
fun Context.createImageFile(): File {
    // Create an image file name
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    val image = File.createTempFile(
        imageFileName, /* prefix */
        ".jpg", /* suffix */
        externalCacheDir      /* directory */
    )
    return image
}

@SuppressLint("DefaultLocale")
fun timeToText(time: Long): String{
    return String.format("%02d:%02d:%02d", time / 3600, (time % 3600) / 60, time % 60)
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomAppBar(drawerState: DrawerState?, title: String, navController: NavHostController, database: AppDatabase, coroutineScope: CoroutineScope, timerViewModel: TimerViewModel, onShowPopUpChange: (Boolean)->Unit) {
    val context = LocalContext.current
    CenterAlignedTopAppBar(
        navigationIcon = {
            if (drawerState != null) {
                IconButton(onClick = {
                    coroutineScope.launch {
                        drawerState.open()
                    }
                }) {
                    Icon(Icons.Filled.Menu, contentDescription = "")
                }
            }
        },
        title = { Text(text = title) },
//        title = {
//            Row(
//                modifier = Modifier.fillMaxWidth(),
//                verticalAlignment = Alignment.CenterVertically
//            ) {
//                Text(text = title)
//                Row(
//                    modifier = Modifier.fillMaxWidth(),
//                    horizontalArrangement = Arrangement.End
//                ) {
//                    Icon(
//                        painter = rememberImagePainter(data = R.drawable.kamil),
//                        contentDescription = "Image",
//                        tint = Color.Unspecified
//                    )
//                }
//            }
//        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
            titleContentColor = MaterialTheme.colorScheme.primary,
        ),
//        actions = {
//            IconButton(onClick = {
//                onShowPopUpChange(true)
//            }) {
//                Icon(Icons.Filled.Search, contentDescription = "")
//            }
//        }
    )
}