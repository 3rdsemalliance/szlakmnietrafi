package edu.gausskkil.szlakmnietrafi

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date

class TimerViewModel : ViewModel() {
    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    private val _timer = MutableStateFlow(0L)
    private var routeId: Int = -1
    val timer = _timer.asStateFlow()

    private var timerJob: Job? = null

    fun startTimer(routeID: Int) {
        routeId = routeID
        timerJob?.cancel()
        timerJob = viewModelScope.launch {
            while (true) {
                delay(1000)
                _timer.value++
            }
        }
    }

    fun pauseTimer() {
        timerJob?.cancel()
    }

    fun stopTimer(database: AppDatabase) {
        Log.i("Database insert", "${database.TimesDAO().genNextIndex()}, ${routeId}, ${_timer.value}, ${dateFormat.format(Date())}")
        database.TimesDAO().insert(RouteTime(database.TimesDAO().genNextIndex(), routeId, _timer.value, dateFormat.format(Date())))
        _timer.value = 0
        timerJob?.cancel()
    }

    override fun onCleared() {
        super.onCleared()
        timerJob?.cancel()
    }
}

@Composable
fun TimerScreenContent(timerViewModel: TimerViewModel, database: AppDatabase, currentRouteID: Int) {
    val timerValue by timerViewModel.timer.collectAsState()

    TimerScreen(
        timerValue = timerValue,
        onStartClick = { timerViewModel.startTimer(currentRouteID) },
        onPauseClick = { timerViewModel.pauseTimer() },
        onStopClick = { timerViewModel.stopTimer(database = database) }
    )
}

@Composable
fun TimerScreen(
    timerValue: Long,
    onStartClick: () -> Unit,
    onPauseClick: () -> Unit,
    onStopClick: () -> Unit
) {
    Text("Zmierz swój czas!",
        modifier = Modifier.fillMaxWidth(), fontSize = 36.sp, textAlign = TextAlign.Center)
    Text(text = timeToText(timerValue), modifier = Modifier.fillMaxWidth(), fontSize = 24.sp, textAlign = TextAlign.Center)
    Row(modifier = Modifier.fillMaxSize(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Center) {
        IconButton(onClick = onStartClick) {
            Icon(rememberPlayArrow(), contentDescription = "Start")
        }
        IconButton(onClick = onPauseClick) {
            Icon(rememberPause(), contentDescription = "Pause")
        }
        IconButton(onClick = onStopClick) {
            Icon(rememberStop(), contentDescription = "Stop")
        }
    }
}