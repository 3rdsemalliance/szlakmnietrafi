package edu.gausskkil.szlakmnietrafi

import SearchbarView
import android.content.res.Configuration
import android.content.res.Resources.Theme
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.TabPosition
import androidx.compose.material3.TabRowDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.adaptive.AnimatedPane
import androidx.compose.material3.adaptive.ExperimentalMaterial3AdaptiveApi
import androidx.compose.material3.adaptive.ListDetailPaneScaffold
import androidx.compose.material3.adaptive.ListDetailPaneScaffoldRole
import androidx.compose.material3.adaptive.rememberListDetailPaneScaffoldNavigator
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.UiComposable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.room.Room
import edu.gausskkil.szlakmnietrafi.ui.theme.SzlakmnietrafiTheme

@OptIn(ExperimentalMaterial3AdaptiveApi::class)
@Composable
fun Main(orientation: Int, database: AppDatabase, timerViewModel: TimerViewModel, tabList: List<TabItem>, navController: NavController){
    val selectedRoute = rememberSaveable(stateSaver = CurrentRoute.Saver){
        mutableStateOf(null)
    }
//    var selectedTabIndex by remember{
//        mutableIntStateOf(0)
//    }
    val selectedTabIndex = rememberSaveable(stateSaver = CurrentTabIndex.Saver){
        mutableStateOf(CurrentTabIndex(0))
    }
    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
//                        Navigate(database, timerViewModel)
            TabLayout(database, timerViewModel, tabList, "vertical", selectedRoute, selectedTabIndex)
        } else {
            TabLayout(database = database, timerViewModel = timerViewModel, items = tabList, style = "horizontal", selectedRoute, selectedTabIndex)
        }
    }
}

@OptIn(ExperimentalMaterial3AdaptiveApi::class)
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "routes.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
        populateDatabase(database)

        val timerViewModel: TimerViewModel by viewModels()

        val tabList = listOf(
            TabItem("O aplikacji"),
            TabItem("Easy"),
            TabItem("Hard")
        )
        setContent {
            SzlakmnietrafiTheme {
                val orientation = this.resources.configuration.orientation
                val navController = rememberNavController()
                SetupNavGraph(navController = navController, orientation = orientation, database = database, timerViewModel = timerViewModel, tabList = tabList)
            }
        }
    }
}
