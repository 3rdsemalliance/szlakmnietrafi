package edu.gausskkil.szlakmnietrafi

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import menus
import DrawerContent
//import NavigationDrawer
import PopupSearchbar
import SearchbarContent
import SearchbarView
import android.util.Log
import androidx.compose.material3.adaptive.ExperimentalMaterial3AdaptiveApi
import androidx.compose.material3.adaptive.rememberListDetailPaneScaffoldNavigator
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.navigation.NavType
import androidx.navigation.navArgument

data class TabItem(
    val title: String
)

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3AdaptiveApi::class
)
@Composable
fun TabLayout(database: AppDatabase, timerViewModel: TimerViewModel, items: List<TabItem>, style: String, selectedRoute: MutableState<CurrentRoute?>, currentTabIndex: MutableState<CurrentTabIndex?>){
    var showPopup by rememberSaveable{
        mutableStateOf(false)
    }
    val pagerState = rememberPagerState {
        items.size
    }
    var selectedTabIndex by remember{
        mutableIntStateOf(0)
    }

    val drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val navController: NavHostController = rememberNavController()
    var changeFlag: Boolean = false

    val navSearchbarController = rememberNavController()
    NavHost(navController = navSearchbarController, startDestination = "DetailLayout/0") {
        composable(
            route = "DetailLayout" + "/{routeID}",
            arguments = listOf(
                navArgument("routeID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routeID")
                ?.let { Log.i("NavChange", "$it")
                    DependentDetailLayout(index = it, modifier = Modifier.fillMaxWidth(), database = database, timerViewModel = timerViewModel) }
        }
    }

    LaunchedEffect(currentTabIndex.value?.id) {
        currentTabIndex.value?.id?.let { pagerState.animateScrollToPage(it) }
    }
    LaunchedEffect(pagerState, pagerState.isScrollInProgress) {
        if(!pagerState.isScrollInProgress){
            selectedTabIndex = pagerState.currentPage
            currentTabIndex.value?.id = pagerState.currentPage
        }
    }

    LaunchedEffect(selectedTabIndex) {
        pagerState.animateScrollToPage(selectedTabIndex)
    }

    val navigator = rememberListDetailPaneScaffoldNavigator()
    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                DrawerContent(menus) { drawerMenu ->
                    coroutineScope.launch {
                        drawerState.close()
                        changeFlag = true
                    }
                    navController.navigate(drawerMenu.route)
                    currentTabIndex.value?.id = drawerMenu.id
                    selectedTabIndex = drawerMenu.id
                }
            }
        },
//        gesturesEnabled = drawerState.isOpen
    ) {
        NavHost(navController = navController, startDestination = MainRoute.About.name) {
            composable(MainRoute.About.name) {
                if(changeFlag){
//                    selectedTabIndex = 0
                    currentTabIndex.value?.id = 0
                    changeFlag = false
                }
//                AboutLayout()
            }
            composable(MainRoute.EasyTrails.name) {
                if(changeFlag){
//                    selectedTabIndex = 1
                    currentTabIndex.value?.id = 1
                    changeFlag = false
                }
//                DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = false)
            }
            composable(MainRoute.HardTrails.name) {
                if(changeFlag){
//                    selectedTabIndex = 2
                    currentTabIndex.value?.id = 2
                    changeFlag = false
                }
//                DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = true)
            }
//            composable(MainRoute.TimeOnTrails.name) {
//                RouteTimesLayout(database = database, navController = navController)
//            }
        }
        Scaffold(
            topBar = {
                CustomAppBar(drawerState = drawerState, title = "Szlak mnie trafi", navController = navController, database = database, coroutineScope = coroutineScope, timerViewModel = timerViewModel, onShowPopUpChange = {showPopup = it})
            },
        ) { innerPadding ->
            Column(
                modifier = Modifier.padding(innerPadding),
                verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                selectedTabIndex?.let {
                    TabRow(selectedTabIndex = it) {
                        items.forEachIndexed { index, item ->
                            Tab(
                                selected = index == it,
                                onClick = {
                                    Log.i("clicked!", "$index")
                                    currentTabIndex.value?.id = index
                                    selectedTabIndex = index
                                },
                                text = {
                                    Text(text = item.title)
                                }
                            )
                        }
                    }
                }
                HorizontalPager(
                    state = pagerState,
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                ) { index ->
                    if(style == "vertical"){
                        when (index) {
                            0 -> AboutLayout()
                            1 -> DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = false, selectedRoute = selectedRoute)
                            2 -> DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = true, selectedRoute = selectedRoute)
                        }
                    }
                    else{
                        when (index) {
                            0 -> AboutLayout()
                            1 -> ListDetailLayout(
                                navigator = navigator,
                                selectedRoute = selectedRoute,
                                database = database,
                                navController = navController,
                                timerViewModel = timerViewModel,
                                hardness = true
                            )
                            2 -> ListDetailLayout(
                                navigator = navigator,
                                selectedRoute = selectedRoute,
                                database = database,
                                navController = navController,
                                timerViewModel = timerViewModel,
                                hardness = false
                            )
                        }
                    }
                }
//            NavigationDrawer(
//                drawerState = drawerState,
//                coroutineScope = coroutineScope,
//                navController = navController,
//                database = database,
//                timerViewModel = timerViewModel
//            )
            }
        }
//        var shownIndex by remember{
//            mutableIntStateOf(-1)
//        }
//        PopupSearchbar(
//            showPopup = showPopup,
//            onClickOutside = { showPopup = false },
//            shownContent = {shownIndex = -1},
//            content = {
//                SearchbarContent(
//                    searchbarView = SearchbarView(database.RouteDAO().getAllRoutes()),
//                    navController = navSearchbarController,
//                    showPopupChange = {showPopup = false}
//                )
//            }
//        )

    }
//    Scaffold(
//        topBar = {
//            CustomAppBar(drawerState = drawerState, title = "Szlak mnie trafi")
//        },
//    ) { innerPadding ->
//        Column(
//            modifier = Modifier.padding(innerPadding),
//            verticalArrangement = Arrangement.spacedBy(4.dp)
//        ) {
//            TabRow(selectedTabIndex = selectedTabIndex) {
//                items.forEachIndexed { index, item ->
//                    Tab(
//                        selected = index == selectedTabIndex,
//                        onClick = {
//                            selectedTabIndex = index
//                        },
//                        text = {
//                            Text(text = item.title)
//                        })
//                }
//            }
//            HorizontalPager(
//                state = pagerState,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .weight(1f)
//            ) { index ->
//                if(style == "vertical"){
//                    when (index) {
//                        0 -> AboutLayout()
//                        1 -> DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = false)
//                        2 -> DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = true)
//                    }
//                }
//                else{
//                    when (index) {
//                        0 -> AboutLayout()
//                        1 -> AboutLayout()
//                        2 -> AboutLayout()
////                        1 -> ListDetailLayout(
////                           navigator = navigator,
////                            selectedRoute = selectedRoute,
////                            database = database,
////                            navController = navController,
////                            timerViewModel = timerViewModel,
////                            hardness = true
////                        )
////                        2 -> ListDetailLayout(
////                            navigator = navigator,
////                            selectedRoute = selectedRoute,
////                            database = database,
////                            navController = navController,
////                            timerViewModel = timerViewModel,
////                            hardness = false
////                        )
//                    }
//                }
//            }
////            NavigationDrawer(
////                drawerState = drawerState,
////                coroutineScope = coroutineScope,
////                navController = navController,
////                database = database,
////                timerViewModel = timerViewModel
////            )
//        }
//    }
}