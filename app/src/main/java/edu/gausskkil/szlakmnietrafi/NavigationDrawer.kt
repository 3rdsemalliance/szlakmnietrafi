import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import kotlinx.coroutines.launch
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.MutableIntState
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.gausskkil.szlakmnietrafi.AboutLayout
import edu.gausskkil.szlakmnietrafi.AppDatabase
import edu.gausskkil.szlakmnietrafi.DependentNavigate
import edu.gausskkil.szlakmnietrafi.Navigate
import edu.gausskkil.szlakmnietrafi.RouteTimesLayout
import edu.gausskkil.szlakmnietrafi.TimerViewModel
import kotlinx.coroutines.CoroutineScope

enum class MainRoute(value: String) {
    About("About"),
    EasyTrails("Easy trails"),
    HardTrails("Hard trails"),
//    TimeOnTrails("Your Trail History")
}

data class DrawerMenu(val icon: ImageVector, val title: String, val route: String, val id: Int)

val menus = arrayOf(
    DrawerMenu(Icons.Filled.Info, "About", MainRoute.About.name, 0),
    DrawerMenu(Icons.Filled.ThumbUp, "Easy Trails", MainRoute.EasyTrails.name, 1),
    DrawerMenu(Icons.Filled.Warning, "Hard Trails", MainRoute.HardTrails.name, 2),
//    DrawerMenu(Icons.Filled.Settings, "Your Trail History", MainRoute.TimeOnTrails.name)
)

@Composable
fun DrawerContent(
    menus: Array<DrawerMenu>,
    onMenuClick: (DrawerMenu) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp),
            contentAlignment = Alignment.Center
        ) {
            Image(
                modifier = Modifier.size(150.dp),
                imageVector = Icons.Filled.AccountCircle,
                contentScale = ContentScale.Crop,
                contentDescription = null
            )
        }
        Spacer(modifier = Modifier.height(12.dp))
        menus.forEach {
            NavigationDrawerItem(
                label = { Text(text = it.title) },
                icon = { Icon(imageVector = it.icon, contentDescription = null) },
                selected = false,
                onClick = {
                    onMenuClick(it)
                }
            )
        }
    }
}

//@Composable
//fun NavigationDrawer(drawerState: DrawerState, coroutineScope: CoroutineScope, navController: NavHostController, database: AppDatabase, timerViewModel: TimerViewModel){
//    ModalNavigationDrawer(
//        drawerState = drawerState,
//        drawerContent = {
//            ModalDrawerSheet {
//                DrawerContent(menus) { route ->
//                    coroutineScope.launch {
//                        drawerState.close()
//                    }
//                    navController.navigate(route)
//                }
//            }
//        },
//        gesturesEnabled = drawerState.isOpen
//    ) {
//        NavHost(navController = navController, startDestination = MainRoute.About.name) {
//            composable(MainRoute.About.name) {
//                AboutLayout()
//            }
//            composable(MainRoute.EasyTrails.name) {
//                DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = false, onSavableRouteIndexChange = {})
//            }
//            composable(MainRoute.HardTrails.name) {
//                DependentNavigate(database = database, timerViewModel = timerViewModel, hardness = true, onSavableRouteIndexChange = {})
//            }
//            composable(MainRoute.TimeOnTrails.name) {
//                RouteTimesLayout(database = database, navController = navController)
//            }
//        }
//    }
//}