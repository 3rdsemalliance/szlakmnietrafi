package edu.gausskkil.szlakmnietrafi

import SearchbarContent
import SearchbarView
import android.annotation.SuppressLint
import android.net.Uri
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DrawerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.FileProvider
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.room.Query
import coil.compose.rememberImagePainter
import edu.gausskkil.szlakmnietrafi.ui.theme.Black
import edu.gausskkil.szlakmnietrafi.ui.theme.Green
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import java.util.Objects

@Composable
fun TrailCard(text: String, modifier: Modifier = Modifier) {
    val ratio = 10
    OutlinedCard(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surface,
        ),
        border = BorderStroke(1.dp, Black),
        modifier = Modifier
            .defaultMinSize(minHeight = LocalConfiguration.current.screenHeightDp.dp / ratio)
    ) {
        Box(
            modifier = modifier.defaultMinSize(minHeight = LocalConfiguration.current.screenHeightDp.dp / ratio)
        ) {
            Column(){
                Image(painter = painterResource(id = R.drawable.logo),
                    contentDescription = null,
                    contentScale = ContentScale.Fit,
                    modifier = Modifier.defaultMinSize(minHeight = LocalConfiguration.current.screenHeightDp.dp / ratio)
                )
                Text(
                    text = text,
                    textAlign = TextAlign.Center,
                    fontSize = 36.sp,
                    lineHeight = 48.sp
                )
            }
        }
    }
}


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun AboutLayout(){
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        item {
            Text(
                "O aplikacji i jej przeznaczeniu",
                modifier = Modifier.padding(10.dp),
                fontSize = 30.sp,
                color = MaterialTheme.colorScheme.primary,
                textAlign = TextAlign.Center
            )
        }
        item {
            HorizontalDivider(modifier = Modifier.width(4.dp))
        }
        item {
            Text(
                "Szlakmnietrafi to aplikacja, " +
                        "której celem jest udostępnienie użytkownikowi najważniejszych informacji " +
                        "o wybranych górskich szlakach. Aby wybrać szlak, należy skierować się " +
                        "do karty drugiej lub trzeciej, które zawierają informacje odpowiednio o " +
                        "szlakach tatrzańskich i nie-tatrzańskich. Aplikacja posiada również stoper do " +
                        "mierzenia czasu przejścia konkretnego szlaku przez użytkownika.",
                modifier = Modifier
                    .padding(horizontal = 20.dp)
                    .padding(bottom = 30.dp),
                color = Green,
                fontSize = 19.sp,
                textAlign = TextAlign.Justify
            )
        }
        item {
            Text(
                "Autorzy: Oskar Kiliańczyk (151863)\ni Kamil Małecki (151861)",
                fontSize = 15.sp,
                color = Green,
                textAlign = TextAlign.Left,
                modifier = Modifier.padding(horizontal = 10.dp)
            )
        }
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun DependentMainLayout(modifier: Modifier = Modifier, navController: NavController, routeList: List<Route>, onClick: ((CurrentRoute) -> Unit)? = null, database: AppDatabase, onSavableRouteIndexChange: (Int)-> Unit) {
    Scaffold (
        topBar = {
            SearchbarContent(searchbarView = SearchbarView(routeList), navController = navController, showPopupChange = {false})
        },

        content = {
            LazyVerticalStaggeredGrid(
                columns = StaggeredGridCells.Fixed(2),
//            verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                items(routeList.size) { index ->
                    if(onClick == null){
                        TrailCard(routeList[index].name,
                            modifier = modifier.clickable{
                                val arg = routeList[index].id.toString()
                                navController.navigate("DetailLayout/$arg")
                                onSavableRouteIndexChange(routeList[index].id)
                            })
                        HorizontalDivider(color = MaterialTheme.colorScheme.outline)
                    }
                    else {
                        TrailCard(
                            routeList[index].name,
                            modifier = modifier.clickable {
                                onClick(CurrentRoute(index))
                            }
                        )
                        HorizontalDivider(color = MaterialTheme.colorScheme.outline)
                    }
                }
            }
        }

    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun DependentDetailLayout(index: Int, modifier: Modifier, database: AppDatabase, timerViewModel: TimerViewModel){
    val context = LocalContext.current
    val file = context.createImageFile()
    val uri = FileProvider.getUriForFile(
        Objects.requireNonNull(context),
        BuildConfig.APPLICATION_ID + ".provider", file
    )
    var capturedImageUri by remember {
        mutableStateOf<Uri>(Uri.EMPTY)
    }
    val cameraLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) {
            capturedImageUri = uri
        }
    val permissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {
        if (it) {
            // Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
            cameraLauncher.launch(uri)
        } else {
            // Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }
    if(index == -1){
        Scaffold(
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { checkPermission(context, cameraLauncher, uri, permissionLauncher) },
                    modifier = Modifier.padding(15.dp)
                ) {
                    Icon(Icons.Filled.Add, "Floating action button.")
                }
            }
        ) { innerPadding ->
            // innerPadding contains inset information for you to use and apply
            Column(
                // consume insets as scaffold doesn't do it by default
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize()
            ) {
                Text("Wybierz szlak", modifier = modifier.fillMaxSize(), fontSize = 36.sp, textAlign = TextAlign.Center)
            }
            FloatingActionButton(
                onClick = { checkPermission(context, cameraLauncher, uri, permissionLauncher) },
            ) {
                Icon(Icons.Filled.Add, "Floating action button.")
            }
        }
    }
    else{
        val routeInfo = database.RouteDAO().getDescForRoute(index)
        val routeTimesList = database.TimesDAO().getDescForRouteTime(index)
        val textPadding = 10.dp
        Scaffold(
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { checkPermission(context, cameraLauncher, uri, permissionLauncher) },
                    modifier = Modifier.padding(15.dp)
                ) {
                    Icon(Icons.Filled.Add, "Floating action button.")
                }
            }
        ) { innerPadding ->
            // innerPadding contains inset information for you to use and apply
            LazyColumn(
                // consume insets as scaffold doesn't do it by default
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                item{
                    Text(routeInfo.name, modifier = modifier, fontSize = 36.sp, textAlign = TextAlign.Center, color = MaterialTheme.colorScheme.error, lineHeight = 48.sp)
                    Text("Opis szlaku", modifier = modifier, fontSize = 30.sp, textAlign = TextAlign.Center)
                    HorizontalDivider(modifier = Modifier.width(4.dp))
                    Text(
                        routeInfo.description,
                        modifier = modifier.padding(textPadding),
                        fontSize = 18.sp,
                        textAlign = TextAlign.Justify,
                    )
                    HorizontalDivider(
                        modifier = Modifier
                            .width(4.dp)
                            .padding(textPadding / 2), color = MaterialTheme.colorScheme.outline
                    )
                    Text("Dystans: ${routeInfo.distance}km", modifier = Modifier
                        .fillMaxWidth()
                        .padding(textPadding / 2), fontSize = 30.sp)
                    Text("Czas: ${routeInfo.time}", modifier = Modifier
                        .fillMaxWidth()
                        .padding(textPadding / 2), fontSize = 30.sp)
                    TimerScreenContent(timerViewModel = timerViewModel, database = database, currentRouteID = index)
                    if (capturedImageUri.path?.isNotEmpty() == true) {
                        Image(
                            modifier = Modifier
                                .padding(16.dp, 8.dp),
                            painter = rememberImagePainter(capturedImageUri),
                            contentDescription = null
                        )
                    }
                }
                item{
                    Text("Zobacz swoje poprzednie podejścia do tego szlaku:", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)
                }
                items(routeTimesList.size) { index ->
                    val routeName = database.RouteDAO().getDescForRoute(routeTimesList[index].routeID).name
                    val date = routeTimesList[index].date
                    val time = timeToText(routeTimesList[index].time)
                    Text("$routeName $date: $time", modifier = Modifier.padding(horizontal = 8.dp))
                }
            }
        }
    }
}

@Composable
fun DependentNavigate(database: AppDatabase, timerViewModel: TimerViewModel, hardness: Boolean, selectedRoute: MutableState<CurrentRoute?>){
    val routeList =
        if (hardness) database.RouteDAO().getRoutesAfterGivenDistance(5.0)
        else database.RouteDAO().getRoutesToGivenDistance(5.0)
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "MainLayout") {
        composable(route = "MainLayout"){
            DependentMainLayout(navController = navController, routeList = routeList, database = database, onSavableRouteIndexChange = { selectedRoute.value = CurrentRoute(it) })
        }
        composable(
            route = "DetailLayout" + "/{routeID}",
            arguments = listOf(
                navArgument("routeID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routeID")
                ?.let { DependentDetailLayout(index = it, modifier = Modifier.fillMaxWidth(), database = database, timerViewModel = timerViewModel) }
        }
    }
}